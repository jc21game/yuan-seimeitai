﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour
{
    public GameObject[] bases; //prefab
    float time = 0;            //次のオブジェクト落とすまでの時間
    float pivotHeight = 15;     //落下位置
    int number = 0;            //ランダムな値を入れる

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;
        if (time <= 0.0f)
        {
            time = 5.0f; //時間入れ直し
            number = Random.Range(0, bases.Length); //どのオブジェクトを落とすかランダムに決める
            int x = Random.Range(-7, 7); //落下位置のX座標をランダムに
            Instantiate(bases[number], new Vector3(x, pivotHeight, 0), Quaternion.identity); //オブジェクト、落下位置(X,Y,Z)、角度
            //pivotHeight += 0.5f; //オブジェクトを生成したら落下位置を上にあげる
        }
    }
}
