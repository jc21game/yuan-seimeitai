﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    Transform tf; //Main CameraのTransform
    Camera cam; //Main CameraのCamera

    void Start()
    {
        tf = this.gameObject.GetComponent<Transform>(); //Main CameraのTransformを取得する。
        cam = this.gameObject.GetComponent<Camera>(); //Main CameraのCameraを取得する。
    }

    void Update()
    {
       // if (Input.GetKey(KeyCode.UpArrow)) //上キーが押されていれば
       // {
       //     tf.position = tf.position + new Vector3(0.0f, 0.2f, 0.0f); //カメラを上へ移動。
       // }
       // if (Input.GetKey(KeyCode.DownArrow)) //下キーが押されていれば
       // {
       //     tf.position = tf.position + new Vector3(0.0f, -0.2f, 0.0f); //カメラを下へ移動。
       // }

       //Clamp();
    }

    private Vector3 player_pos;

    void Clamp()
    {
        player_pos = transform.position; //プレイヤーの位置を取得

        player_pos.y = Mathf.Clamp(player_pos.y, 1.0f, 100.0f); //x位置が常に範囲内か監視
        transform.position = new Vector3(0, player_pos.y, -30); //範囲内であれば常にその位置がそのまま入る
    }
}