﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour
{
    public GameObject[] bases; //prefab
    float time = 0;            //次のオブジェクト落とすまでの時間
    float pivotHeight = 20;    //落下位置
    int number = 0;            //ランダムな値を入れる
    public AudioClip[] sound;
    AudioSource audioSource;

    float countdown = 3f;
    int count;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(countdown>=0)
        {
            countdown -= Time.deltaTime;
            count = (int)countdown;
        }
        if(countdown<=0)
        {
            FallingObject();
        }
      
    }

    void FallingObject()
    {
        time -= Time.deltaTime;
        if (time <= 0.0f)
        {
            //audioSource.PlayOneShot(sound[0]); //ドラムロール
          
            time = 5.0f; //時間入れ直し

            //どのオブジェクトを落とすかランダムに決める
            number = Random.Range(0, bases.Length);

            //落下位置のX座標をランダムに この場合だと-26から26の間のどこかに落ちる
            int x = Random.Range(-26, 26);

            //オブジェクト、落下位置(X,Y,Z)、角度
            Instantiate(bases[number], new Vector3(x, pivotHeight, 0), Quaternion.identity);

            //オブジェクトを生成したら落下位置を上にあげる(重ねていくと落下位置に積み上げた物が到達してしまうため)
            pivotHeight += 0.3f; 
        }
    }

}
