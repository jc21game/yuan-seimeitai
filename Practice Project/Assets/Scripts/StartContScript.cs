﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartContScript : MonoBehaviour
{
    public Text CountText;
    float countdown=4f;
    int count;

    public AudioClip sound;
    AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        //ここに処理
        audioSource.PlayOneShot(sound);
    }

    // Update is called once per frame
    void Update()
    {
        if(countdown>=0)
        {
            countdown -= Time.deltaTime;
            count = (int)countdown;
            CountText.text = count.ToString();
        }
        if(countdown<=0)
        {
            CountText.text = "";
        }
    }
    
}
