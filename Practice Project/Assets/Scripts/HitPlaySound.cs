﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitPlaySound : MonoBehaviour
{
    public AudioClip[] sound;
    AudioSource audioSource;
    int HitCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.PlayOneShot(sound[0]);
    }

    // Update is called once per frame
    void Update()
    {
        DeleteObject();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        HitCount++;
        if(HitCount==1)
        {
            audioSource.PlayOneShot(sound[1]);
        }
    }

    void DeleteObject()
    {
        //-12未満でオブジェクト削除
        if (transform.position.y < -30)
        {
            Destroy(gameObject);

        }
    }
}
