﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public AudioClip sound;
    AudioSource audioSource;

    //カウントダウンBGM用
    float STcountdown = 3f;
    int count;

    //カウントダウン
    public float countdown = 60.0f;

    //時間を表示するText型の変数
    public Text timeText;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        Invoke("ContdownBGM", 29);
    }

    // Update is called once per frame
    void Update()
    {
        if (STcountdown >= 0)
        {
            STcountdown -= Time.deltaTime;
            count = (int)countdown;
        }
        if (STcountdown <= 0)
        {
            //時間をカウントダウンする
            countdown -= Time.deltaTime;

            //時間を表示する
            timeText.text = countdown.ToString("f0");
        }

        //countdownが0以下になったとき
        if (countdown <= 0)
        {
            //リザルトに移行するとかの処理を書く
            SceneManager.LoadScene("ResultScene");
            // timeText.text = "時間になりました！";
        }
    }

    void ContdownBGM()
    {
     
        //ここに処理
        audioSource.PlayOneShot(sound);

    }
}